use std::ffi::CString;
use gl::{self, types::*};

use crate::types::{Rect, Color};

const VS_SRC: &'static [u8] = b"
#version 330 core

layout (location = 0) in vec2 position;
layout (location = 1) in vec4 color;

out vec4 v_color;

void main() {
    v_color = color;
    gl_Position = vec4(position, 0.0, 1.0);
}
\0";

const FS_SRC: &'static [u8] = b"
#version 330 core

in vec4 v_color;
out vec4 frag_color;

void main() {
    frag_color = v_color;
}
\0";

pub struct Graphics {
    window_width: f32,
    window_height: f32,
}

impl Graphics {

    pub fn new(window_width: u32, window_height: u32) -> Self {
        Self {
            window_width: window_width as f32,
            window_height: window_height as f32,
        }
    }

    pub fn create_shader(&self, shader_type: GLuint, source: &[u8]) -> GLuint {
        unsafe {
            let id = gl::CreateShader(shader_type);
            gl::ShaderSource(
                id,
                1,
                [source.as_ptr() as *const _].as_ptr(),
                std::ptr::null()
            );
            gl::CompileShader(id);
            let mut success: GLint = 1;
            gl::GetShaderiv(id, gl::COMPILE_STATUS, &mut success);
            if success == 0 {
                let mut len: GLint = 0;
                gl::GetShaderiv(id, gl::INFO_LOG_LENGTH, &mut len);
                let error = {
                    let mut buffer: Vec<u8> = Vec::with_capacity(len as usize + 1);
                    buffer.extend([b' '].iter().cycle().take(len as usize));
                    CString::from_vec_unchecked(buffer)
                };
                gl::GetShaderInfoLog(id, len, std::ptr::null_mut(), error.as_ptr() as *mut GLchar);
                eprintln!("{}", error.to_string_lossy());
            }
            id
        }
    }

    pub fn create_program(&self, vert_src: &[u8], frag_src: &[u8]) -> GLuint {
        let vert = self.create_shader(gl::VERTEX_SHADER, vert_src);
        let frag = self.create_shader(gl::FRAGMENT_SHADER, frag_src);
        unsafe {
            let program = gl::CreateProgram();
            gl::AttachShader(program, vert);
            gl::AttachShader(program, frag);
            gl::LinkProgram(program);
            gl::DeleteShader(vert);
            gl::DeleteShader(frag);
            let mut success: GLint = gl::FALSE as GLint;
            gl::GetProgramiv(program, gl::LINK_STATUS, &mut success);
            if success == gl::FALSE as GLint {
                let mut len: GLint = 0;
                gl::GetProgramiv(program, gl::INFO_LOG_LENGTH, &mut len);
                let error = {
                    let mut buffer: Vec<u8> = Vec::with_capacity(len as usize + 1);
                    buffer.extend([b' '].iter().cycle().take(len as usize));
                    CString::from_vec_unchecked(buffer)
                };
                gl::GetProgramInfoLog(program, len, std::ptr::null_mut(), error.as_ptr() as *mut gl::types::GLchar);
                eprintln!("{}", error.to_string_lossy());
            }
            program
        }
    }

    pub fn clear(&self, c: Color) {
        unsafe {
            gl::ClearColor(
                c.r as GLfloat / 255.0 as GLfloat,
                c.g as GLfloat / 255.0 as GLfloat,
                c.b as GLfloat / 255.0 as GLfloat,
                c.a as GLfloat / 255.0 as GLfloat,
            );
            gl::Clear(gl::COLOR_BUFFER_BIT);
        }
    }

    pub fn draw_rects(&self, rects: &Vec<Rect>, color: Color) {

        // let mut rects = &rects[0..1];

        for rect in rects {
            println!("Rect {} {} {} {}", rect.x, rect.y, rect.w, rect.h);
        }

        // let mut rects = Vec::new();
        // rects.push(Rect {
        //     x: 0.0,
        //     y: 0.0,
        //     w: 15.0,
        //     h: 15.0,
        // });

        let x_min = rects.iter().fold(f32::MAX, |acc, r| acc.min(r.x.min(r.x + r.w)));
        let x_max = rects.iter().fold(f32::MIN, |acc, r| acc.max(r.x.max(r.x + r.w)));
        let y_min = rects.iter().fold(f32::MAX, |acc, r| acc.min(r.y.min(r.y + r.h)));
        let y_max = rects.iter().fold(f32::MIN, |acc, r| acc.max(r.y.max(r.y + r.h)));

        println!("Bounds: ({x_min}, {y_min}) to ({x_max}, {y_max})");

        let width = x_max - x_min;
        let height = y_max - y_min;

        let size = width.max(height) / 2.0;
        let size_x = size;
        let size_y = size / self.window_width * self.window_height;
        // let size = 10.0;

        let mut vertices: Vec<f32> = Vec::new();
        for rect in rects {
            // let x1 = rect.x / width * 2.0 - 1.0;
            // let x2 = (rect.x + rect.w) / width * 2.0 - 1.0;
            // let y1 = rect.y / height * 2.0 - 1.0;
            // let y2 = (rect.y + rect.h) / height * 2.0 - 1.0;
            let x1 = rect.x / size_x - (1.0);
            let y1 = -rect.y / size_y;
            let x2 = (rect.x + rect.w) / size_x - (1.0);
            let y2 = -(rect.y + rect.h) / size_y;

            println!("Verts ({x1} {y1}) to ({x2}, {y2})");

            let rect_verts = [
                (x1, y1),
                (x2, y1),
                (x2, y2),
                (x1, y1),
                (x2, y2),
                (x1, y2),
            ];

            for (x, y) in &rect_verts {
                vertices.push(*x);
                vertices.push(*y);
                vertices.push(color.r as GLfloat / 255.0);
                vertices.push(color.g as GLfloat / 255.0);
                vertices.push(color.b as GLfloat / 255.0);
                vertices.push(color.a as GLfloat / 255.0);
            }
        }

        unsafe {

            let program = self.create_program(VS_SRC, FS_SRC);

            let mut vbo = 0;
            let mut vao = 0;

            let float_size = std::mem::size_of::<GLfloat>();

            gl::GenBuffers(1, &mut vbo);
            gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
            gl::BufferData(
                gl::ARRAY_BUFFER,
                (vertices.len() * float_size) as GLsizeiptr,
                vertices.as_ptr() as *const _,
                gl::STATIC_DRAW,
            );

            let stride = 6 * float_size;
            gl::GenVertexArrays(1, &mut vao);
            gl::BindVertexArray(vao);
            gl::EnableVertexAttribArray(0);
            gl::EnableVertexAttribArray(1);
            gl::VertexAttribPointer(0, 2, gl::FLOAT, gl::FALSE, stride as i32, 0 as *const _);
            gl::VertexAttribPointer(1, 4, gl::FLOAT, gl::FALSE, stride as i32, (2 * float_size) as *const _);

            gl::UseProgram(program);
            gl::DrawArrays(gl::TRIANGLES, 0, (vertices.len()) as GLint);
        }
    }

    pub fn resize(&mut self, width: u32, height: u32) {
        unsafe {
            gl::Viewport(0, 0, width as GLint, height as GLint);
        }
        self.window_width = width as f32;
        self.window_height = height as f32;
    }
}

