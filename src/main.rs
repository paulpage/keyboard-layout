use serde_json::Value;
use glutin::event::{Event, WindowEvent};
use glutin::event_loop::{ControlFlow, EventLoop};
use glutin::window::WindowBuilder;
use glutin::ContextBuilder;
use glutin::dpi::PhysicalSize;

mod types;
use types::{Rect, Color};
mod graphics;
use graphics::Graphics;

fn parse_kle_json(data: &str) -> Option<Vec<Rect>> {
    let json: Value = serde_json::from_str(&data)
        .expect("JSON parsing error");

    if let Value::Array(rows) = json {

        let mut rects: Vec<Rect> = Vec::new();

        let mut rect: Rect = Rect {
            x: 0.0,
            y: 0.0,
            w: 1.0,
            h: 1.0,
        };

        for row in rows {
            if let Value::Array(entries) = row {
                for entry in entries {
                    if let Value::String(_) = entry {
                        // Make the keys spaced out
                        let final_rect = Rect {
                            x: rect.x,
                            y: rect.y,
                            w: rect.w - 0.1,
                            h: rect.h - 0.1,
                        };
                        rects.push(final_rect);
                        rect.x += rect.w;

                        // TODO is this right?
                        rect.w = 1.0;
                        rect.h = 1.0;
                    } else if let Value::Object(o) = entry {
                        for (k, v) in &o {
                            if let Value::Number(n) = v {
                                let n = n.as_f64().unwrap() as f32;
                                rect.w = 1.0;
                                rect.h = 1.0;
                                match k.as_str() {
                                    "x" => rect.x += n,
                                    "y" => rect.y += n,
                                    "w" => rect.w = n,
                                    "h" => rect.h = n,
                                    _ => (),
                                }

                            }
                        }
                    }
                }
                rect.y += 1.0;
                rect.x = 0.0;
            }
        }
        return Some(rects);
    }
    None
}

fn main() {
    let data = std::fs::read_to_string("/home/paul/Downloads/keyboard-layout.json")
        .expect("Failed to read file");

    let rects = parse_kle_json(&data).unwrap();

    let event_loop = EventLoop::new();
    let window_builder: WindowBuilder = WindowBuilder::new().with_inner_size(PhysicalSize::new(800, 600)).with_title("Keyboard Layout Editor");
    let windowed_context = ContextBuilder::new().build_windowed(window_builder, &event_loop).unwrap();
    let windowed_context = unsafe { windowed_context.make_current().unwrap() };
    gl_loader::init_gl();
    gl::load_with(|symbol| gl_loader::get_proc_address(symbol) as *const _);

    let mut graphics = Graphics::new(800, 600);

    event_loop.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::Wait;

        match event {
            Event::LoopDestroyed => {
                gl_loader::end_gl();
                return;
            },
            Event::WindowEvent { event, .. } => match event {
                WindowEvent::Resized(physical_size) => {
                    windowed_context.resize(physical_size);
                    graphics.resize(physical_size.width, physical_size.height);
                }
                WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
                _ => (),
            },
            Event::RedrawRequested(_) => {
                graphics.clear(Color::new(0, 100, 0, 255));
                graphics.draw_rects(&rects, Color::new(0, 0, 100, 255));
                windowed_context.swap_buffers().unwrap();
            }
            _ => (),
        }
    });
}
